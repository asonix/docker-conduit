ARG REPO_ARCH

FROM $REPO_ARCH/alpine:edge as base

ARG UID=991
ARG GID=991

RUN \
  addgroup --gid "${GID}" app && \
  adduser -D -G app -u "${UID}" -g "" -h /opt/app app && \
  apk add tini libgcc libstdc++ musl

FROM base as builder

RUN \
  apk add clang15 clang15-dev linux-headers gcc g++ make cargo rust rustfmt

USER app

COPY --chown=app:app . /conduit

WORKDIR /conduit

RUN \
  cargo build --release --no-default-features --features conduit_bin,backend_rocksdb && \
  strip target/release/conduit

FROM base as runner

COPY --from=builder /conduit/target/release/conduit /usr/local/bin/conduit

ENV CONDUIT_CONFIG="/opt/app/conduit.toml"
USER app
ENTRYPOINT [ "/sbin/tini", "--" ]
CMD [ "/usr/local/bin/conduit" ]
